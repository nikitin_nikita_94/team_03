package ru.edu.project.frontend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.group.GroupService;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.api.students.StudentForm;
import ru.edu.project.backend.api.students.StudentService;

import javax.validation.Valid;

@Controller
@RequestMapping("/manager")
public class ManagerController {

    /**
     * Атрибут модели для хранения списка студентов.
     */
    public static final String STUDENT = "students";

    /**
     * Атрибут модели для хранения списка групп.
     */
    public static final String GROUP = "groups";

    /**
     * Атрибут BindingResult.
     */
    public static final String ERROR = "ERROR";

    /**
     * Внедрение зависимости StudentService.
     */
    @Autowired
    private StudentService studentService;

    /**
     * Внедрение зависимости StudentService.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Начальная страница.
     *
     * @param model
     * @return String
     */
    @GetMapping("/")
    public String index(final Model model) {
        model.addAttribute(STUDENT, studentService.getAllStudents());

        return "manager/student/index";
    }

    /**
     * Показ карточки User по id.
     *
     * @param studentId
     * @return ModelAndView
     */
    @GetMapping("/view/{id}")
    public ModelAndView view(final @PathVariable("id") Long studentId) {
        ModelAndView model = new ModelAndView("manager/student/view");

        Student student = studentService.getStudentById(studentId);
        if (student == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("client/student/viewNotFound");
            return model;
        }

        model.addObject(STUDENT, student);
        return model;
    }

    /**
     * Создать User.
     *
     * @param model
     * @return String
     */
    @GetMapping("/create")
    public String createForm(final Model model) {
        model.addAttribute(STUDENT, studentService.getAllStudents());
        return "manager/student/create";
    }

    /**
     * Создать User.
     *
     * @param createForm
     * @param bindingResult
     * @param model
     * @return String
     */
    @PostMapping("/create")
    public String createFormProcessing(
            @Valid @ModelAttribute final StudentForm createForm,
            final BindingResult bindingResult,
            final Model model
    ) {
        if (bindingResult.hasErrors()) {
            model.addAttribute(ERROR, bindingResult.getAllErrors());
            return createForm(model);
        }

        Group group = groupService.getByIdGroup(createForm.getGroup());
        if (group == null) {
            return "redirect:/manager/";
        }

        Student student = Student.builder()
                .firstName(createForm.getFirstName())
                .lastName(createForm.getLastName())
                .group(Group.builder().id(createForm.getGroup()).build())
                .phoneNumber(createForm.getPhoneNumber())
                .email(createForm.getEmail())
                .build();

        studentService.saveStudent(student);
        return "redirect:/manager/";
    }

    /**
     * Удалить User.
     *
     * @param studentId
     * @return String
     */
    @GetMapping("/delete/{id}")
    public String deleteStudent(final @PathVariable("id") Long studentId) {
        studentService.deleteStudentById(studentId);
        return "redirect:/manager/";
    }

    /**
     * Начальная страница GroupService.
     *
     * @param model
     * @return String
     */
    @GetMapping("/group")
    public String groupIndex(final Model model) {
        model.addAttribute(GROUP, groupService.getAllGroup());
        return "manager/group/index_group";
    }

    /**
     * Отображение Group.
     *
     * @param groupId
     * @return ModelAndView
     */
    @GetMapping("/group/view/{id}")
    public ModelAndView viewGroup(final @PathVariable("id") Long groupId) {
        ModelAndView model = new ModelAndView("manager/group/view_group");
        Group group = groupService.getByIdGroup(groupId);
        if (group == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("client/student/viewNotFound");
            return model;
        }

        model.addObject(GROUP, group);
        return model;
    }

    /**
     * Создание Group.
     *
     * @param model
     * @return String
     */
    @GetMapping("/group/create")
    public String createGroup(final Model model) {
        model.addAttribute(GROUP, groupService.getAllGroup());
        return "manager/group/create_group";
    }

    /**
     * Создание Group.
     *
     * @param title
     * @return String
     */
    @PostMapping("/group/create")
    public String createGroupProcessing(@Valid final String title) {
        groupService.createGroup(title);
        return "redirect:/manager/group";
    }

    /**
     * Удаление Group on id.
     *
     * @param id
     * @return String
     */
    @GetMapping("/group/delete/{id}")
    public String deleteGroup(final @PathVariable("id") Long id) {
        groupService.removeGroup(id);
        return "redirect:/manager/group";
    }
}
