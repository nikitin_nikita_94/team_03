package ru.edu.project.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.stub.lessons.InMemoryStubLessonService;
import ru.edu.project.backend.stub.students.InMemoryStubStudentService;
import ru.edu.project.backend.stub.tasks.InMemoryStubTaskService;


@Configuration
@Profile("STUBS")
public class StubsConfig {


    /**
     * Заглушка сервиса LessonService.
     *
     * @return bean
     */
    @Bean
    public LessonService lessonServiceBean() {
        return new InMemoryStubLessonService();
    }

    /**
     * Заглушка сервиса TaskService.
     *
     * @param lessonService
     * @return bean
     */
    @Bean
    public TaskService taskServiceBean(final LessonService lessonService) {
        return new InMemoryStubTaskService(lessonService);
    }

    /**
     * Заглушка сервиса StudentService.
     *
     * @return bean
     */
    public StudentService requestServiceBean() {
        return new InMemoryStubStudentService();
    }
}
