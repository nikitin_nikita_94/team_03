package ru.edu.project.frontend.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class TeacherControllerTest {

    public static final Long STUDENT_ID = 11L;

    @InjectMocks
    private TeacherController teacherController;

    @Mock
    private TaskService taskService;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void view() {
        long taskId = 111L;

        TaskInfo expectedTaskInfo = TaskInfo.builder().build();
        when(taskService.getDetailedInfo(STUDENT_ID, taskId)).thenReturn(expectedTaskInfo);

        ModelAndView view = teacherController.view(taskId);

        assertEquals("teacher/view", view.getViewName());
    }
}
