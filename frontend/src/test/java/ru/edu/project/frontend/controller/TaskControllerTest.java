package ru.edu.project.frontend.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.MockitoAnnotations.openMocks;

public class TaskControllerTest {

    public static final Long STUDENT_ID = 11L;

    @Mock
    private TaskService taskService;

    @Mock
    private Model modelMock;

    @Mock
    private LessonService lessonService;

    @InjectMocks
    private ClientController taskController;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void createForm() {
        ArrayList<Lesson> expectedLessonList = new ArrayList<>();

        Mockito.when(lessonService.getAvailable()).thenReturn(expectedLessonList);

        String viewName = taskController.createForm(modelMock);
        assertEquals("client/task/create_task", viewName);
        Mockito.verify(modelMock).addAttribute("lessons", expectedLessonList);
    }

    @Test
    public void createFormProcessing() {
        BindingResult bindingResultMock = Mockito.mock(BindingResult.class);
        Mockito.when(bindingResultMock.hasErrors()).thenReturn(false);

        TaskInfo expectedInfo = TaskInfo.builder().id(111L).build();

        ClientController.CreateForm createForm = new ClientController.CreateForm();
        createForm.setComment("commentStr");

        Mockito.when(taskService.createTask(Mockito.any(TaskForm.class))).thenAnswer(invocation -> {
            TaskForm form = invocation.getArgument(0, TaskForm.class);

            assertEquals(STUDENT_ID, form.getStudentId());
            assertEquals(createForm.getTaskContent(),form.getTaskContent());
            assertEquals(createForm.getComment(), form.getComment());
            assertEquals(createForm.getLessons(), form.getSelectedLessons());
            return expectedInfo;
        });

    }
}
