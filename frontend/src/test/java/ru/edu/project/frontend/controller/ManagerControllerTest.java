package ru.edu.project.frontend.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.group.GroupService;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.api.students.StudentForm;
import ru.edu.project.backend.api.students.StudentService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.MockitoAnnotations.openMocks;

public class ManagerControllerTest {

    @InjectMocks
    private ManagerController managerController;

    @Mock
    private StudentService studentService;

    @Mock
    private Model modelMock;

    @Mock
    private GroupService groupService;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void index() {
        List<Student> expectedList = new ArrayList<>();
        Mockito.when(studentService.getAllStudents()).thenReturn(expectedList);

        Model modelMock = Mockito.mock(Model.class);
        String viewName = managerController.index(modelMock);

        assertEquals("manager/student/index",viewName);
        Mockito.verify(modelMock).addAttribute("students",expectedList);
    }

    @Test
    public void view() {
        long studentId = 100L;

        Student studentExpected = Student.builder().build();
        Mockito.when(studentService.getStudentById(studentId)).thenReturn(studentExpected);

        ModelAndView view = managerController.view(studentId);

        assertEquals("manager/student/view", view.getViewName());
        assertEquals(studentExpected, view.getModel().get("students"));
    }

    @Test
    public void createForm() {
        List<Student> expectedList = new ArrayList<>();
        Mockito.when(studentService.getAllStudents()).thenReturn(expectedList);

        Model modelMock = Mockito.mock(Model.class);
        String viewName = managerController.createForm(modelMock);

        assertEquals("manager/student/create",viewName);
        Mockito.verify(modelMock).addAttribute("students",expectedList);
    }

    @Test
    public void createFormProcessing() {
        BindingResult bindingResultMock = Mockito.mock(BindingResult.class);
        Mockito.when(bindingResultMock.hasErrors()).thenReturn(false);

        Student expectedStudent = Student.builder().build();
        StudentForm createStudent = Mockito.mock(StudentForm.class);

        Mockito.when(studentService.saveStudent(any(Student.class))).thenReturn(expectedStudent);

        String viewName = managerController.createFormProcessing(createStudent, bindingResultMock, modelMock);
        assertEquals("redirect:/manager/", viewName);

    }

    @Test
    public void createFormProcessingHasErrors(){
        List<ObjectError> mockErrors = new ArrayList<>();
        BindingResult bindingResultMock = Mockito.mock(BindingResult.class);
        Mockito.when(bindingResultMock.hasErrors()).thenReturn(true);
        Mockito.when(bindingResultMock.getAllErrors()).thenReturn(mockErrors);

        ArrayList<Student> expectedStudentList = new ArrayList<>();
        Mockito.when(studentService.getAllStudents()).thenReturn(expectedStudentList);

        String viewName = managerController.createFormProcessing(
                new StudentForm(),
                bindingResultMock,
                modelMock
        );

        assertEquals("manager/student/create", viewName);
        Mockito.verify(modelMock).addAttribute("students", expectedStudentList);
        Mockito.verify(modelMock).addAttribute("ERROR", mockErrors);
    }

    @Test
    public void deleteStudent() {
        long clientId = 10L;
        Student student = Mockito.mock(Student.class);
        Mockito.when(studentService.getStudentById(clientId)).thenReturn(student);

        String view = managerController.deleteStudent(clientId);
        assertEquals("redirect:/manager/", view);
    }

    @Test
    public void groupIndex() {
        List<Group> expectedGroup = new ArrayList<>();
        Mockito.when(groupService.getAllGroup()).thenReturn(expectedGroup);

        Model modelMock = Mockito.mock(Model.class);
        String view = managerController.groupIndex(modelMock);

        assertEquals("manager/group/index_group",view);
        Mockito.verify(modelMock).addAttribute("groups",expectedGroup);
    }

    @Test
    public void viewGroup() {
        long groupId = 100L;

        Group groupExpected = Group.builder().build();
        Mockito.when(groupService.getByIdGroup(groupId)).thenReturn(groupExpected);

        ModelAndView view = managerController.viewGroup(groupId);

        assertEquals("manager/group/view_group", view.getViewName());
        assertEquals(groupExpected, view.getModel().get("groups"));
    }

    @Test
    public void createGroup() {
        List<Group> expectedGroup = new ArrayList<>();
        Mockito.when(groupService.getAllGroup()).thenReturn(expectedGroup);

        Model modelMock = Mockito.mock(Model.class);
        String view = managerController.createGroup(modelMock);

        assertEquals("manager/group/create_group",view);
        Mockito.verify(modelMock).addAttribute("groups",expectedGroup);
    }

    @Test
    public void createGroupProcessing() {
        BindingResult bindingResultMock = Mockito.mock(BindingResult.class);
        Mockito.when(bindingResultMock.hasErrors()).thenReturn(false);

        Group expectedGroup = Group.builder().build();
        String expected = "expected";

        Mockito.when(groupService.createGroup(expected)).thenReturn(expectedGroup);

        String viewName = managerController.createGroupProcessing(expected);
        assertEquals("redirect:/manager/group", viewName);

        Mockito.verify(modelMock, Mockito.times(0)).addAttribute(anyString(), any());
        Mockito.verify(groupService).createGroup(anyString());
    }

    @Test
    public void deleteGroup() {
        long clientId = 10L;
        String view = managerController.deleteGroup(clientId);
        assertEquals("redirect:/manager/group", view);
    }
}