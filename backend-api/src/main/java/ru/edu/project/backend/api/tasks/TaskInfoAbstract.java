package ru.edu.project.backend.api.tasks;

public interface TaskInfoAbstract {

 /**
  * Получение id выполненного задания.
  *
  * @return id
  */
    Long getId();

 /**
  * Получение id студента.
  *
  * @return studentId
  */
   Long getStudentId();

 /**
  * Получение текста выполненного задания.
  *
  * @return taskContent
  */
    String getTaskContent();

 /**
  * Получение пояснения.
  *
  * @return comment
  */
    String getComment();

 /**
  * Получение времени создания.
  *
  * @return createAt
  */
    java.sql.Timestamp getCreatedAt();

 /**
  * Получение времени последнего внесения изменения.
  *
  * @return lastActionAt
  */
    java.sql.Timestamp getLastActionAt();

 /**
  * Получение времени проверки.
  *
  * @return checkAt
  */
    java.sql.Timestamp getCheckAt();

 /**
  * Получение оценки.
  *
  * @return score
  */
    ru.edu.project.backend.api.common.Score getScore();

 /**
  * Получение занятий.
  *
  * @return services
  */
    java.util.List<ru.edu.project.backend.api.lessons.Lesson> getServices();

 /**
  * Получение истории действий.
  *
  * @return actionHistory
  */
    java.util.List<ru.edu.project.backend.api.action.Action> getActionHistory();
}
