package ru.edu.project.backend.api.lessons;

public interface LessonAbstract {

    /**
     * Получение id занятия.
     *
     * @return long
     */
    Long getId();

    /**
     * Получение названия занятия.
     *
     * @return string
     */
    String getTitle();

    /**
     * Получение содержания занятия.
     *
     * @return string
     */
    String getDescription();

    /**
     * Получение задания к занятию.
     *
     * @return string
     */
    String getIssue();
}
