package ru.edu.project.backend.api.common;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = SimpleScore.class)
public interface Score {

    /**
     * Код оценки.
     *
     * @return scoreCode
     */
    Long getScoreCode();

    /**
     * Значение оценки.
     *
     * @return score
     */
    String getScoreValue();
}
