package ru.edu.project.backend.api.tasks;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Getter
@Builder
@Jacksonized
public class TaskForm {

    /**
     * id студента.
     */
    private Long studentId;

    /**
     * Выбранные занятия.
     */
    private List<Long> selectedLessons;

    /**
     * Текст выполненного задания.
     */
    private String taskContent;

    /**
     * Пояснение.
     */
    private String comment;
}
