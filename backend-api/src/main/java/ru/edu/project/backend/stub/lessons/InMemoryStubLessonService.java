package ru.edu.project.backend.stub.lessons;

import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.lessons.LessonService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class InMemoryStubLessonService implements LessonService {

    /**
     * Получение доступных занятий.
     *
     * @return список
     */
    @Override
    public List<Lesson> getAvailable() {
        return Arrays.stream(LessonsEnum.values())
                .map(LessonsEnum::getLesson)
                .collect(Collectors.toList());
    }

    /**
     * Получение занятий по коду.
     *
     * @param ids
     * @return список
     */
    @Override
    public List<Lesson> getByIds(final List<Long> ids) {
        return Arrays.stream(LessonsEnum.values())
                .filter(e -> ids.contains(e.getLesson().getId()))
                .map(LessonsEnum::getLesson)
                .collect(Collectors.toList());
    }

    /**
     * Расширение через Enum.
     */
    public enum LessonsEnum {

        /**
         * Отладочное занятие.
         */
        LESSON_1(1L, "Занятие 1"),

        /**
         * Отладочное занятие.
         */
        LESSON_2(2L, "Занятие 2"),

        /**
         * Отладочное занятие.
         */
        LESSON_3(3L, "Занятие 3"),

        /**
         * Отладочное занятие.
         */
        LESSON_4(4L, "Занятие 4");


        /**
         * Связанный объект.
         */
        private Lesson lesson;

        LessonsEnum(final Long code, final String title) {
            lesson = Lesson.builder()
                    .id(code)
                    .title(title)
                    .build();
        }

        /**
         * Получение объекта.
         *
         * @return lesson
         */
        public Lesson getLesson() {
            return lesson;
        }
    }

}
