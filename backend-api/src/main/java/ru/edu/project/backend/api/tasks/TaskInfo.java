package ru.edu.project.backend.api.tasks;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;
import ru.edu.project.backend.api.action.Action;
import ru.edu.project.backend.api.common.Score;
import ru.edu.project.backend.api.lessons.Lesson;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@Builder
@Jacksonized
public class TaskInfo implements TaskInfoAbstract {

    /**
     * id выполненного задания.
     */
    private Long id;

    /**
     * id студента.
     */
    private Long studentId;

    /**
     * Текст выполненного задания.
     */
    private String taskContent;

    /**
     * Пояснение.
     */
    private String comment;

    /**
     * Время создания.
     */
    private Timestamp createdAt;

    /**
     * Время последнего внесения изменения.
     */
    private Timestamp lastActionAt;

    /**
     * Время проверки преподавателем.
     */
    private Timestamp checkAt;

    /**
     * Оценка.
     */
    private Score score;

    /**
     * Занятия.
     */
    private List<Lesson> services;

    /**
     * История действий.
     */
    private List<Action> actionHistory;
}
