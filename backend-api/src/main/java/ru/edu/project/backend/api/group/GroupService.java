package ru.edu.project.backend.api.group;

import ru.edu.project.backend.api.common.AcceptorArgument;
import ru.edu.project.backend.api.students.Student;

import java.util.List;

public interface GroupService {

    /**
     * Возвращает группу по Id.
     *
     * @param id
     * @return Group
     */
    Group getByIdGroup(Long id);

    /**
     * Возвращает список студентов одной группы.
     * @param id
     * @return List
     */
    List<Student> getStudentsByGroup(Long id);

    /**
     * Создание группы.
     *
     * @param name
     * @return Group
     */
    @AcceptorArgument
    Group createGroup(String name);

    /**
     * Удалить группу.
     * @param id
     */
    void removeGroup(Long id);

    /**
     * Вернуть все группы.
     *
     * @return List
     */
    List<Group> getAllGroup();
}
