package ru.edu.project.backend.api.common;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class ScoreImpl implements Score {

    /**
     * Код оценки.
     */
    private Long scoreCode;

    /**
     * Значение оценки.
     */
   private String scoreValue;
}
