package ru.edu.project.backend.api.lessons;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class Lesson implements LessonAbstract {

    /**
     * id занятия.
     */
    private Long id;

    /**
     * Название занятия.
     */
    private String title;

    /**
     * Содержание занятия.
     */
    private String description;

    /**
     * Задание к занятию.
     */
    private String issue;
}
