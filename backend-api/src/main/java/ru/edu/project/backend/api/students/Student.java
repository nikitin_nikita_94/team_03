package ru.edu.project.backend.api.students;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;
import ru.edu.project.backend.api.group.Group;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Jacksonized
public class Student {

    /**
     * ID студента.
     */
    private Long studentId;

    /**
     * Имя студента.
     */
    private String firstName;

    /**
     * Фамилия студента.
     */
    private String lastName;

    /**
     * ID группы.
     */
    private Group group;

    /**
     * Средний балл.
     */
    private Float progress;

    /**
     * Окончил ли курс студент.
     */
    private Boolean isGraduated;

    /**
     * Номер телефона студента.
     */
    private String phoneNumber;

    /**
     * Почта студента.
     */
    private String email;
}
