package ru.edu.project.backend.api.action;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface ActionService {

    /**
     * Поиск действий по выполненному заданию.
     *
     * @param taskId
     * @return list
     */
    List<Action> searchByTask(long taskId);

    /**
     * Создание действия для выполненного задания.
     *
     * @param createActionTask
     * @return list
     */
    @AcceptorArgument
    Action createAction(CreateActionTask createActionTask);
}
