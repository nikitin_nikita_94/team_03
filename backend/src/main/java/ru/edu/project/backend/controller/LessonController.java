package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.service.LessonServiceLayer;

import java.util.List;

@RestController
@RequestMapping("/lesson")
public class LessonController implements LessonService {


    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private LessonServiceLayer delegate;

    /**
     * Получение доступных занятий.
     *
     * @return список
     */
    @Override
    @GetMapping("/getAvailable")
    public List<Lesson> getAvailable() {
        return delegate.getAvailable();
    }

    /**
     * Получение занятий по коду.
     *
     * @param ids
     * @return list
     */
    @Override
    @PostMapping("/getByIds")
    public List<Lesson> getByIds(final @RequestBody List<Long> ids) {
        return delegate.getByIds(ids);
    }
}
