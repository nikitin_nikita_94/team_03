package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.TaskEntity;

import java.util.List;

@Repository
public interface TaskEntityRepository extends PagingAndSortingRepository<TaskEntity, Long>,
        JpaSpecificationExecutor<TaskEntity> {

    /**
     * Поиск записей по полю student_id.
     *
     * @param studentId
     * @return list
     */
    List<TaskEntity> findByStudentId(Long studentId);
}
