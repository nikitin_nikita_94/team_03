package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import ru.edu.project.backend.da.jpa.entity.ActionEntity;

import java.util.List;

public interface ActionEntityRepository extends CrudRepository<ActionEntity, ActionEntity.ActionPk> {

    /**
     * Поиск действий по выполненному заданию.
     *
     * @param taskId
     * @return list
     */
    List<ActionEntity> findAllByPkTaskId(long taskId);
}
