package ru.edu.project.backend.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.edu.project.backend.api.common.Score;

import java.util.Arrays;

@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TaskScore implements Score {

    /**
     * Оценка по умолчанию.
     */
    WAIT_CHECK(1L, " "),

    /**
     * Отрицательная оценка.
     */
    NOT_APPROVED(3L, "Незачет"),

    /**
     * Положительная оценка.
     */
    APPROVED(2L, "Зачет");

    /**
     * Код оценки.
     */
    private final Long scoreCode;

    /**
     * Значение.
     */
    private final String scoreValue;

    /**
     * Получение enum оценки по ее коду.
     *
     * @param score
     * @return enum or null
     */
    public static Score byScoreCode(final long score) {
        return Arrays.stream(values())
                .filter(s -> s.getScoreCode() == score)
                .findFirst().orElse(null);
    }
}

