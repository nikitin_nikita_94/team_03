package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.group.GroupService;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.da.GroupDaLayer;

import java.util.List;

@Service
@Profile("!STUB")
@Qualifier("GroupServiceLayer")
public class GroupServiceLayer implements GroupService {

    /**
     * Зависимость для слоя доступа к данным.
     */
    @Autowired
    private GroupDaLayer delegate;

    /**
     * Возвращает группу по Id.
     *
     * @param id
     * @return Group
     */
    @Override
    public Group getByIdGroup(final Long id) {
        return delegate.getByIdGroup(id);
    }

    /**
     * Возвращает список студентов одной группы.
     *
     * @param id
     * @return List
     */
    @Override
    public List<Student> getStudentsByGroup(final Long id) {
        return delegate.getStudentsByGroup(id);
    }

    /**
     * Создание группы.
     *
     * @param name
     * @return Group
     */
    @Override
    public Group createGroup(final String name) {
        return delegate.createGroup(name);
    }

    /**
     * Удалить группу.
     *
     * @param id
     */
    @Override
    public void removeGroup(final Long id) {
        delegate.removeGroup(id);
    }


    /**
     * Вернуть все группы.
     *
     * @return List
     */
    @Override
    public List<Group> getAllGroup() {
        return delegate.getAllGroup();
    }
}
