package ru.edu.project.backend.da.jdbctemplate;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.students.Student;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Data Access слой для student_table.
 */
@Slf4j
@Component
@Profile("JDBC_TEMPLATE")
public class StudentDA implements ru.edu.project.backend.da.StudentDALayer {

    /**
     * Вернуть всех студентов.
     */
    public static final String SELECT_ALL_STUDENT = "select * from students";

    /**
     * Вернуть студента по указаному ID.
     */
    public static final String SELECT_BY_ID = "select * from students where id = ?";

    /**
     * Обновить запись.
     */
    public static final String QUERY_FOR_UPDATE = "update students set first_name = :first_name,"
                                                + " last_name = :last_name, group_id=:group_id,"
                                                + " average_scope=:progress,is_graduated = :is_graduated,"
                                                + "phone_number=:phone_number,email=:email where id = :id";

    /**
     * Удаление студента по ID.
     */
    public static final String DELETE_BY_ID = "delete from students where id = ?";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;


    /**
     * Зависимость на шаблон jdbc insert.
     */
    private SimpleJdbcInsert jdbcInsert;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;


    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean
     */
    @Autowired
    public void setJdbcInsert(final SimpleJdbcInsert bean) {
        jdbcInsert = bean
                .withTableName("students")
                .usingGeneratedKeyColumns("id");
    }

    /**
     * Возвращает список студентов.
     *
     * @return List
     */
    @Override
    public List<Student> getAllStudents() {
        return jdbcTemplate.query(SELECT_ALL_STUDENT, this::rowMapper);
    }

    /**
     * Сохраняет запись.
     * Или обновляет.
     *
     * @param student
     * @return Student
     */
    @Override
    public Student saveStudent(final Student student) {
        if (student.getStudentId() == null) {
            return insert(student);
        }
        return update(student);
    }

    /**
     * Обновляет студента в таблице.
     *
     * @param student
     * @return Student
     */
    private Student update(final Student student) {
        int status = jdbcNamed.update(QUERY_FOR_UPDATE, toMap(student));
        if (status != 0) {
            log.info("Student data updated for ID " + student.getStudentId());
        } else {
            log.info("No Student found with ID " + student.getStudentId());
        }
        return student;
    }


    /**
     * Вставка студента в таблицу.
     *
     * @param student
     * @return Student
     */
    private Student insert(final Student student) {
        long id = jdbcInsert.executeAndReturnKey(toMap(student)).longValue();
        student.setStudentId(id);
        return student;
    }

    private Map<String, Object> toMap(final Student student) {
        HashMap<String, Object> map = new HashMap<>();
        if (student.getStudentId() != null) {
            map.put("student_id", student.getStudentId());
        }
        map.put("first_name", student.getFirstName());
        map.put("last_name", student.getLastName());
        map.put("group_id", student.getGroup());
        map.put("progress", student.getProgress());
        map.put("is_graduated", student.getIsGraduated());
        map.put("phone_number", student.getPhoneNumber());
        map.put("email", student.getEmail());

        return map;
    }

    /**
     * Возвращает студента по указанному ID.
     *
     * @param id
     * @return Student
     */
    @Override
    public Student getStudentById(final Long id) {
        return jdbcTemplate.query(SELECT_BY_ID, this::singleRowMapper, id);
    }

    /**
     * Удаляет студента по указаному ID.
     *
     * @param id
     */
    @Override
    public void deleteStudentById(final Long id) {
        int status = jdbcTemplate.update(DELETE_BY_ID, id);
        if (status != 0) {
            log.info("Student data deleted for ID " + id);
        } else {
            log.info("No Student found with ID " + id);
        }
    }

    /**
     * Сопостовляет строки.
     *
     * @param resultSet
     * @param pos
     * @return Student
     */
    @SneakyThrows
    private Student rowMapper(final ResultSet resultSet, final int pos) {
        return mapRow(resultSet);
    }

    @SneakyThrows
    private Student singleRowMapper(final ResultSet resultSet) {
        resultSet.next();
        return mapRow(resultSet);
    }

    private Student mapRow(final ResultSet resultSet) throws SQLException {
        return Student.builder()
                .studentId(resultSet.getLong("student_id"))
                .firstName(resultSet.getString("first_name"))
                .lastName(resultSet.getString("last_name"))
                .group(resultSet.getObject("group_id", Group.class))
                .progress(resultSet.getFloat("progress"))
                .isGraduated(resultSet.getBoolean("is_graduated"))
                .phoneNumber(resultSet.getString("phone_number"))
                .email(resultSet.getString("email"))
                .build();
    }
}
