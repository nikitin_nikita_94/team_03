package ru.edu.project.backend.da.jpa;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.converter.GroupConverter;
import ru.edu.project.backend.converter.StudentConverter;
import ru.edu.project.backend.da.GroupDaLayer;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;
import ru.edu.project.backend.da.jpa.repository.GroupEntityRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@Profile("SPRING_DATA")
public class JPAGroupDa implements GroupDaLayer {

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private GroupEntityRepository repo;

    /**
     * Конвертер.
     */
    @Autowired
    private GroupConverter groupConverter;

    /**
     * Конвертер.
     */
    @Autowired
    private StudentConverter studentConverter;

    /**
     * Возвращает группу по Id.
     *
     * @param id
     * @return Group
     */
    @Override
    public Group getByIdGroup(final Long id) {
        Optional<GroupEntity> groupEntity = repo.findById(id);
        GroupEntity groupEntityById = groupConverter.getGroupEntityById(groupEntity);
        return groupConverter.buildGroup(groupEntityById);
    }

    /**
     * Создание группы.
     *
     * @param name
     * @return Group
     */
    @Override
    public Group createGroup(final String name) {
        return groupConverter.buildGroup(repo.save(GroupEntity
                .builder()
                .title(name)
                .studentList(new ArrayList<>())
                .build()));
    }

    /**
     * Удалить группу.
     *
     * @param id
     */
    @Override
    public void removeGroup(final Long id) {
        repo.deleteById(id);
    }

    /**
     * Вернуть список всех студентов одной группы.
     *
     * @param id
     * @return List
     */
    @Override
    public List<Student> getStudentsByGroup(final Long id) {
        Optional<GroupEntity> byGroupEntityId = repo.findById(id);
        GroupEntity entityById = groupConverter.getGroupEntityById(byGroupEntityId);
        return entityById.getStudentList().stream()
                .map(student -> studentConverter.buildStudent(student))
                .collect(Collectors.toList());
    }

    /**
     * Вернуть все группы.
     *
     * @return List
     */
    @Override
    public List<Group> getAllGroup() {
        return groupConverter.map(repo.findAll());
    }
}
