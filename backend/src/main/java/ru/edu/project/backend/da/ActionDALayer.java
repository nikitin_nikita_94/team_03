package ru.edu.project.backend.da;

import ru.edu.project.backend.api.action.Action;

import java.util.List;

public interface ActionDALayer {

    /**
     * Поиск действий по сданной работе.
     *
     * @param taskId
     * @return list
     */
    List<Action> findByTask(long taskId);

    /**
     * Создание действия.
     *
     * @param taskId
     * @param build
     * @return action
     */
    Action save(long taskId, Action build);
}
