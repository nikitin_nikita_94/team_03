package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.common.PageView;
import ru.edu.project.backend.api.common.Search;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.da.TaskDALayer;
import ru.edu.project.backend.da.jpa.converter.TaskInfoMapper;
import ru.edu.project.backend.da.jpa.entity.TaskEntity;
import ru.edu.project.backend.da.jpa.repository.TaskEntityRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Profile("SPRING_DATA")
public class JPATaskDA implements TaskDALayer {

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private TaskEntityRepository repo;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private TaskInfoMapper mapper;

    /**
     * Получение списка выполненных заданий по studentId.
     *
     * @param studentId
     * @return list
     */
    @Override
    public List<TaskInfo> getStudentTasks(final long studentId) {
        return mapper.mapList(repo.findByStudentId(studentId));
    }

    /**
     * Получение выполненного задания по id.
     *
     * @param id
     * @return info
     */
    @Override
    public TaskInfo getById(final long id) {
        Optional<TaskEntity> entity = repo.findById(id);
        return entity.map(taskEntity -> mapper.map(taskEntity))
                .orElse(null);
    }

    /**
     * Сохранение (создание/обновление) выполненного задания.
     *
     * @param draft
     * @return info
     */
    @Override
    public TaskInfo save(final TaskInfo draft) {
       TaskEntity entity = mapper.map(draft);

        TaskEntity saved = repo.save(entity);
        draft.setId(saved.getId());
        return mapper.map(saved);
    }

    /**
     * Поиск выполненных заданий.
     *
     * @param search
     * @return list
     */
    @Override
    public PageView<TaskInfo> searchTask(final Search search) {
        Sort.Direction direction = search
                .isAscendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC;

        PageRequest pageRequest = PageRequest.of(search.getPage(), search.getPerPage(),
                Sort.by(direction, search.getSortBy()));

        Page<TaskEntity> page = repo.findAll(pageRequest);

        return PageView.<TaskInfo>builder()
                .elements(mapper.mapList(page.get().collect(Collectors.toList())))
                .page(search.getPage())
                .perPage(search.getPerPage())
                .total(page.getTotalElements())
                .totalPages(page.getTotalPages())
                .build();
    }
}
