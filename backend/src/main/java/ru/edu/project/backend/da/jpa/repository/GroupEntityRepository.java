package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;

public interface GroupEntityRepository extends CrudRepository<GroupEntity, Long> {
}
