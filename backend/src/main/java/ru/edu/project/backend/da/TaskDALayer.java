package ru.edu.project.backend.da;

import ru.edu.project.backend.api.common.PageView;
import ru.edu.project.backend.api.common.Search;
import ru.edu.project.backend.api.tasks.TaskInfo;

import java.util.List;

public interface TaskDALayer {

    /**
     * Получение списка выполненных заданий по studentId.
     *
     * @param studentId
     * @return list task
     */
    List<TaskInfo> getStudentTasks(long studentId);

    /**
     * Получение выполненных заданий по id.
     *
     * @param id
     * @return task
     */
    TaskInfo getById(long id);

    /**
     * Сохранение (создание/обновление) выполненного задания.
     *
     * @param draft
     * @return task
     */
    TaskInfo save(TaskInfo draft);

    /**
     * Поиск выполненных заданий.
     *
     * @param search
     * @return list
     */
    PageView<TaskInfo> searchTask(Search search);
}
