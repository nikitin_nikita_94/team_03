package ru.edu.project.backend.da.jdbctemplate;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.edu.project.backend.api.lessons.Lesson;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Data Access слой для lesson_table.
 */
@Component
@Profile("JDBC_TEMPLATE")
public class LessonDA implements ru.edu.project.backend.da.LessonDALayer {

    /**
     * Запрос для поиска всех доступных занятий.
     */
    public static final String QUERY_AVAILABLE = "SELECT * FROM lesson WHERE enabled = true";

    /**
     * Запрос для поиска всех доступных занятий по списку id.
     */
    public static final String QUERY_AVAILABLE_BY_IDS = "SELECT * FROM lesson WHERE id IN (:ids)";

    /**
     * Запрос для поиска всех доступных занятий по id.
     */
    public static final String QUERY_AVAILABLE_BY_ID = "SELECT * FROM lesson WHERE enabled = true AND id = ?";

    /**
     * Запрос для связки выполненного задания студента и занятия.
     */
    public static final String QUERY_FOR_LINK = "INSERT INTO lesson_link (task_id, lesson_id) VALUES(?, ?)";

    /**
     * Запрос поиска типов занятий привязанных к заданию студента.
     */
    public static final String QUERY_TYPE_BY_LINK_TASK_ID = "SELECT t.* FROM lesson_link l LEFT JOIN lesson t ON l.lesson_id = t.id WHERE l.task_id = ?";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Связываем выполненное задание и занятие.
     *
     * @param taskId
     * @param lessonId
     */
    @Override
    public void linkTask(final long taskId, final long lessonId) {
        jdbcTemplate.update(QUERY_FOR_LINK, taskId, lessonId);
    }

    /**
     * Получаем занятие по id.
     *
     * @param id
     * @return lesson
     */
    @Override
    public Lesson getById(final Long id) {
        return jdbcTemplate.query(QUERY_AVAILABLE_BY_ID, this::singleRowMapper, id);
    }

    /**
     * Получаем список занятий по списку id.
     *
     * @param ids
     * @return list
     */
    @Override
    public List<Lesson> getByIds(final List<Long> ids) {
        Map<String, Object> map = new HashMap<>();
        map.put("ids", ids);
        return jdbcNamed.query(QUERY_AVAILABLE_BY_IDS, map, this::rowMapper);
    }

    /**
     * Получаем список доступных занятий.
     *
     * @return list lesson
     */
    @Override
    public List<Lesson> getAvailable() {
        return jdbcTemplate.query(QUERY_AVAILABLE, this::rowMapper);
    }

    /**
     * Получаем список занятий связанных с заданием.
     *
     * @param taskId
     * @return list
     */
    @Override
    public List<Lesson> getLinksByTaskId(final long taskId) {
        return jdbcTemplate.query(QUERY_TYPE_BY_LINK_TASK_ID, this::rowMapper, taskId);
    }

    @SneakyThrows
    private Lesson rowMapper(final ResultSet resultSet, final int pos) {
        return mapRow(resultSet);
    }

    @SneakyThrows
    private Lesson singleRowMapper(final ResultSet resultSet) {
        resultSet.next();
        return mapRow(resultSet);
    }

    private Lesson mapRow(final ResultSet resultSet) throws SQLException {
        return Lesson.builder()
                .id(resultSet.getLong("id"))
                .title(resultSet.getString("title"))
                .description(resultSet.getString("description"))
                .issue(resultSet.getString("issue"))
                .build();
    }
}
