package ru.edu.project.backend.da.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "students")
public class StudentEntity {
    /**
     * ID студента.
     */
    @Id
    @Column(name = "student_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "stud_seq")
    @SequenceGenerator(name = "stud_seq", sequenceName = "stud_id_sequence", allocationSize = 1)
    private Long studentId;

    /**
     * Имя студента.
     */
    @Column(name = "first_name")
    private String firstName;

    /**
     * Фамилия студента.
     */
    @Column(name = "last_name")
    private String lastName;

    /**
     * ID группы.
     */
    @ManyToOne
    @JoinColumn(name = "group_id", nullable = false)
    private GroupEntity group;

    /**
     * Средний балл.
     */
    @Column(name = "progress")
    private Float progress;

    /**
     * Окончил ли курс студент.
     */
    @Column(name = "is_graduated")
    private Boolean isGraduated;

    /**
     * Номер телефона студента.
     */
    @Column(name = "phone_number")
    private String phoneNumber;

    /**
     * Почта студента.
     */
    @Column(name = "email")
    private String email;
}
