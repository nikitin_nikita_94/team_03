package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.action.Action;
import ru.edu.project.backend.api.action.ActionService;
import ru.edu.project.backend.api.action.CreateActionTask;
import ru.edu.project.backend.service.ActionServiceLayer;

import java.util.List;

@RestController
@RequestMapping("/action")
public class ActionController implements ActionService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private ActionServiceLayer delegate;

    /**
     * Поиск действий по выполненному заданию.
     *
     * @param taskId
     * @return list
     */
    @Override
    @GetMapping("/searchByTask/{taskId}")
    public List<Action> searchByTask(@PathVariable("taskId") final long taskId) {
        return delegate.searchByTask(taskId);
    }

    /**
     * Создание действия.
     *
     * @param createActionTask
     * @return list
     */
    @Override
    @PostMapping("/createAction")
    public Action createAction(@RequestBody final CreateActionTask createActionTask) {
        return delegate.createAction(createActionTask);
    }
}
