package ru.edu.project.backend.da;

import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.students.Student;

import java.util.List;

public interface GroupDaLayer {

    /**
     * Возвращает группу по Id.
     *
     * @param id
     * @return Group
     */
    Group getByIdGroup(Long id);

    /**
     * Создание группы.
     *
     * @param name
     * @return Group
     */
    Group createGroup(String name);

    /**
     * Удалить группу.
     * @param id
     */
    void removeGroup(Long id);

    /**
     * Возвращает список студентов одной группы.
     * @param id
     * @return List
     */
    List<Student> getStudentsByGroup(Long id);

    /**
     * Вернуть все группы.
     *
     * @return List
     */
    List<Group> getAllGroup();
}
