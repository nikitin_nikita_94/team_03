package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.da.LessonDALayer;
import ru.edu.project.backend.da.jpa.converter.LessonMapper;
import ru.edu.project.backend.da.jpa.entity.LessonEntity;
import ru.edu.project.backend.da.jpa.entity.LessonLinkEntity;
import ru.edu.project.backend.da.jpa.repository.LessonEntityRepository;
import ru.edu.project.backend.da.jpa.repository.LessonLinkEntityRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Profile("SPRING_DATA")
public class JPALessonDA implements LessonDALayer {

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private LessonEntityRepository lessonRepo;

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private LessonLinkEntityRepository linkRepo;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private LessonMapper lessonMapper;

    /**
     * Связывание выполненного задания и занятия.
     *
     * @param taskId
     * @param lessonId
     */
    @Override
    public void linkTask(final long taskId, final long lessonId) {
        LessonLinkEntity entityDraft = new LessonLinkEntity();
        entityDraft.setPk(LessonLinkEntity.pk(taskId, lessonId));
        entityDraft.setLesson(getLessonEntityById(lessonId));

        linkRepo.save(entityDraft);
    }

    private LessonEntity getLessonEntityById(final Long id) {
        Optional<LessonEntity> lessonEntity = lessonRepo.findById(id);
        if (!lessonEntity.isPresent()) {
            throw new RuntimeException("Lesson not found by lessonId " + id);
        }
        return lessonEntity.get();
    }

    /**
     * Получение занятия по id.
     *
     * @param id
     * @return lesson
     */
    @Override
    public Lesson getById(final Long id) {
        return lessonMapper.map(getLessonEntityById(id));
    }

    /**
     * Получение списка занятий по ids.
     *
     * @param ids
     * @return list
     */
    @Override
    public List<Lesson> getByIds(final List<Long> ids) {
        return lessonMapper.map(lessonRepo.findAllById(ids));
    }

    /**
     * Получение списка доступных занятий.
     *
     * @return list
     */
    @Override
    public List<Lesson> getAvailable() {
        return lessonMapper.map(lessonRepo.findAllByEnabled(true));
    }

    /**
     * Получение списка занятий по taskId.
     *
     * @param taskId
     * @return list
     */
    @Override
    public List<Lesson> getLinksByTaskId(final long taskId) {
        List<LessonLinkEntity> lessonsEntities = linkRepo.findAllByPkTaskId(taskId);
        return lessonMapper.map(lessonsEntities
                .stream().map(LessonLinkEntity::getLesson)
                .collect(Collectors.toList()));
    }
}
