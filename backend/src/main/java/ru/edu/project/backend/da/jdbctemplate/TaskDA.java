package ru.edu.project.backend.da.jdbctemplate;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.common.PageView;
import ru.edu.project.backend.api.common.Search;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.model.TaskScore;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Data Access слой для task_table.
 */
@Service
@Profile("JDBC_TEMPLATE")
public class TaskDA implements ru.edu.project.backend.da.TaskDALayer {

    /**
     * Запрос для поиска выполненных заданий по id студента.
     */
    public static final String QUERY_FOR_STUDENT_ID = "SELECT * FROM task WHERE student_id = ?";

    /**
     * Запрос для поиска выполненных заданий по id.
     */
    public static final String QUERY_FOR_ID = "SELECT * FROM task WHERE id = ?";

    /**
     * Обновление информации о выполненном задании.
     * Обновляем только изменяемые поля.
     */
    public static final String QUERY_FOR_UPDATE = "UPDATE task SET score = :score, last_action_time = :last_action_time, check_time = :check_time, comment = :comment WHERE id = :id";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;


    /**
     * Зависимость на шаблон jdbc insert.
     */
    private SimpleJdbcInsert jdbcInsert;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean
     */
    @Autowired
    public void setJdbcInsert(final SimpleJdbcInsert bean) {
        jdbcInsert = bean
                .withTableName("task")
                .usingGeneratedKeyColumns("id");
    }

    /**
     * Поиск выполненных заданий студента.
     *
     * @param studentId
     * @return list task
     */
    @Override
    public List<TaskInfo> getStudentTasks(final long studentId) {
        return jdbcTemplate.query(QUERY_FOR_STUDENT_ID, this::rowMapper, studentId);
    }

    /**
     * Поиск выполненного задания студента.
     *
     * @param id
     * @return task
     */
    @Override
    public TaskInfo getById(final long id) {
        return jdbcTemplate.query(QUERY_FOR_ID, this::singleRowMapper, id);
    }

    /**
     * Сохранение выполненного задания.
     *
     * @param draft
     * @return info
     */
    @Override
    public TaskInfo save(final TaskInfo draft) {
        if (draft.getId() == null) {
            return insert(draft);
        }
        return update(draft);
    }

    /**
     * Поиск выполненных заданий.
     *
     * @param search
     * @return info
     */
    @Override
    public PageView<TaskInfo> searchTask(final Search search) {
        throw new RuntimeException("Need to implement ru.edu.project.backend.da.jdbctemplate.TaskDA.searchTask");
    }

    private TaskInfo update(final TaskInfo draft) {
        jdbcNamed.update(QUERY_FOR_UPDATE, toMap(draft));
        return draft;
    }


    private TaskInfo insert(final TaskInfo draft) {
        long id = jdbcInsert.executeAndReturnKey(toMap(draft)).longValue();
        draft.setId(id);
        return draft;
    }

    private Map<String, Object> toMap(final TaskInfo draft) {
        HashMap<String, Object> map = new HashMap<>();
        if (draft.getId() != null) {
            map.put("id", draft.getId());
        }

        map.put("student_id", draft.getStudentId());
        map.put("task_content", draft.getTaskContent());
        map.put("comment", draft.getComment());
        map.put("created_time", draft.getCreatedAt());
        map.put("last_action_time", draft.getLastActionAt());
        map.put("check_time", draft.getCheckAt());
        map.put("score", draft.getScore().getScoreCode());

        return map;
    }


    @SneakyThrows
    private TaskInfo rowMapper(final ResultSet rs, final int pos) {
        return mapRow(rs);
    }

    @SneakyThrows
    private TaskInfo singleRowMapper(final ResultSet rs) {
        rs.next();
        return mapRow(rs);
    }

    private TaskInfo mapRow(final ResultSet rs) throws SQLException {
        return TaskInfo.builder()
                .id(rs.getLong("id"))
                .studentId(rs.getLong("student_id"))
                .taskContent(rs.getString("task_content"))
                .comment(rs.getString("comment"))
                .createdAt(rs.getTimestamp("created_time"))
                .lastActionAt(rs.getTimestamp("last_action_time"))
                .checkAt(rs.getTimestamp("check_time"))
                .score(TaskScore.byScoreCode(rs.getLong("score")))
                .build();
    }
}
