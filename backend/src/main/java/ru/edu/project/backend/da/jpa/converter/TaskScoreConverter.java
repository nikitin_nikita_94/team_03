package ru.edu.project.backend.da.jpa.converter;

import ru.edu.project.backend.api.common.Score;
import ru.edu.project.backend.model.TaskScore;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class TaskScoreConverter implements AttributeConverter<Score, Long> {

    /**
     * Конвертация Score -> Long.
     *
     * @param score
     * @return long
     */
    @Override
    public Long convertToDatabaseColumn(final Score score) {
        if (score == null) {
            return null;
        }
        return score.getScoreCode();
    }

    /**
     * Конвертация Long -> TaskScore enum.
     *
     * @param longScoreValue
     * @return enum
     */
    @Override
    public Score convertToEntityAttribute(final Long longScoreValue) {
        return TaskScore.byScoreCode(longScoreValue);
    }
}
