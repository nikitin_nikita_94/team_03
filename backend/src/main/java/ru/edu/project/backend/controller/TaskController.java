package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.common.PageView;
import ru.edu.project.backend.api.common.Search;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.tasks.UpdateScoreTask;

import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController implements TaskService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private TaskService delegate;

    /**
     * Получение выполненных заданий студента.
     *
     * @param id
     * @return list
     */
    @Override
    @GetMapping("/getTaskByStudent/{id}")
    public List<TaskInfo> getTaskByStudent(@PathVariable("id") final long id) {
        return delegate.getTaskByStudent(id);
    }

    /**
     * Получение детальной информации по выполненному студентом заданию.
     *
     * @param studentId
     * @param taskId
     * @return info
     */
    @Override
    @GetMapping("/getDetailedInfo/{studentId}/{taskId}")
    public TaskInfo getDetailedInfo(
            @PathVariable("studentId") final long studentId,
            @PathVariable("taskId") final long taskId) {
        return delegate.getDetailedInfo(studentId, taskId);
    }

    /**
     * Получение детальной информации по выполненному заданию.
     *
     * @param taskId
     * @return info
     */
    @Override
    @GetMapping("/getDetailedInfo/{taskId}")
    public TaskInfo getDetailedInfo(
            @PathVariable("taskId") final long taskId) {
        return delegate.getDetailedInfo(taskId);
    }

    /**
     * Регистрация нового выполненного задания.
     *
     * @param taskForm
     * @return info
     */
    @Override
    @PostMapping("/createTask")
    public TaskInfo createTask(@RequestBody final TaskForm taskForm) {
        return delegate.createTask(taskForm);
    }

    /**
     * Поиск сданных работ.
     *
     * @param search
     * @return list
     */
    @Override
    @PostMapping("/searchTasks")
    public  PageView<TaskInfo> searchTasks(@RequestBody final Search search) {
        return delegate.searchTasks(search);
    }

    /**
     * Выставление оценки.
     *
     * @param updateScoreTask
     * @return boolean
     */
    @Override
    @PostMapping("/updateScore")
    public boolean updateScore(@RequestBody final UpdateScoreTask updateScoreTask) {
        return delegate.updateScore(updateScoreTask);
    }

















}
