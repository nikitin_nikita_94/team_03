package ru.edu.project.backend.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;
import ru.edu.project.backend.api.action.Action;
import ru.edu.project.backend.api.common.Score;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.tasks.TaskInfoAbstract;
import ru.edu.project.backend.da.jpa.converter.TaskScoreConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "TASK")
public class TaskEntity implements TaskInfoAbstract {

    /**
     * id выполненного задания.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "task_seq")
    @SequenceGenerator(name = "task_seq", sequenceName = "task_id_sequence", allocationSize = 1)
    private Long id;

    /**
     * id студента.
     */
    private Long studentId;

    /**
     * Текст выполненного задания.
     */
    private String taskContent;

    /**
     * Пояснение.
     */
    private String comment;

    /**
     * Время создания.
     */
    @Column(name = "created_time")
    private Timestamp createdAt;

    /**
     * Время последнего внесения изменения.
     */
    @Column(name = "last_action_time")
    private Timestamp lastActionAt;

    /**
     * Время проверки преподавателем.
     */
    @Column(name = "check_time")
    private Timestamp checkAt;

    /**
     * Оценка.
     */
    @Convert(converter = TaskScoreConverter.class)
    private Score score;

    /**
     * Получение занятий.
     *
     * @return list
     */
    @Override
    public List<Lesson> getServices() {
        return null;
    }

    /**
     * Получение истории действий.
     *
     * @return list
     */
    @Override
    public List<Action> getActionHistory() {
        return null;
    }
}
