package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.da.LessonDALayer;

import java.util.List;

@Service
@Profile("!STUB")
@Qualifier("LessonServiceLayer")
public class LessonServiceLayer implements LessonService {

    /**
     * Зависимость для слоя доступа к данным занятий.
     */
    @Autowired
    private LessonDALayer daLayer;

    /**
     * Получение доступных занятий.
     *
     * @return список
     */
    @Override
    public List<Lesson> getAvailable() {
        return daLayer.getAvailable();
    }

    /**
     * Получение занятий по коду.
     *
     * @param ids
     * @return список
     */
    @Override
    public List<Lesson> getByIds(final List<Long> ids) {
        return daLayer.getByIds(ids);
    }

    /**
     * Получение занятий связанных с выполненными заданиями по taskId.
     *
     * @param taskId
     * @return list lesson
     */
    public List<Lesson> getByLink(final long taskId) {
        return daLayer.getLinksByTaskId(taskId);
    }

    /**
     * Связывание выполненного задания и занятия.
     *
     * @param taskId
     * @param ids
     */
    public void link(final long taskId, final List<Long> ids) {
        ids.forEach(id -> daLayer.linkTask(taskId, id));
    }
}
