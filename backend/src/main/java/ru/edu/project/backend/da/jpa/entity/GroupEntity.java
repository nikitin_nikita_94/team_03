package ru.edu.project.backend.da.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "[GROUP]")
public class GroupEntity {

    /**
     * Первичный ключ.
     */
    @Id
    @Column(name = "group_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "group_seq")
    @SequenceGenerator(name = "group_seq", sequenceName = "group_id_sequence", allocationSize = 1)
    private Long groupId;

    /**
     * Заголовок группы.
     */
    @Column(name = "title")
    private String title;

    /**
     * Список студентов группы.
     */
    @OneToMany
    @JoinColumn(name = "group_id", referencedColumnName = "group_id")
    private List<StudentEntity> studentList;
}
