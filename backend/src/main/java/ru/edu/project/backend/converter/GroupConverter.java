package ru.edu.project.backend.converter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Component
public class GroupConverter {

    /**
     * Converter.
     */
    @Autowired
    private StudentConverter converter;

    /**
     * GroupEntity -> Group.
     *
     * @param entity
     * @return Group
     */
    public Group buildGroup(final GroupEntity entity) {
        return Group.builder()
                .id(entity.getGroupId())
                .title(entity.getTitle())
                .studentList(entity.getStudentList()
                        .stream()
                        .map(student -> converter.buildStudent(student))
                        .collect(Collectors.toList()))
                .build();
    }

    /**
     * Вспомогательный метод.
     *
     * @param byGroupEntityId
     * @return GroupEntity
     */
    public GroupEntity getGroupEntityById(final Optional<GroupEntity> byGroupEntityId) {
        if (!byGroupEntityId.isPresent()) {
            log.info("Group not found by groupId " + byGroupEntityId.get().getGroupId());
        }
        return byGroupEntityId.get();
    }

    /**
     * Iterable<GroupEntity> -> List<Group>.
     *
     * @param groups
     * @return List
     */
    public List<Group> map(final Iterable<GroupEntity> groups) {
        return StreamSupport.stream(groups.spliterator(), false)
                .map(this::buildGroup)
                .collect(Collectors.toList());
    }
}
