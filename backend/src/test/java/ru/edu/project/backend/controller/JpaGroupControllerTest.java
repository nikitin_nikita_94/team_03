package ru.edu.project.backend.controller;

import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.edu.project.backend.api.group.Group;
import ru.edu.project.backend.api.students.Student;
import ru.edu.project.backend.service.GroupServiceLayer;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(JpaGroupController.class)
public class JpaGroupControllerTest {

    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private JpaGroupController controller;

    @Mock
    private GroupServiceLayer delegate;

    private Group group;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(controller).build();
        group = Group.builder().id(1L).title("Group").build();
    }

    @Test
    @SneakyThrows
    public void getByIdGroup() {
        given(delegate.getByIdGroup(any(Long.class))).willReturn(group);
        mvc.perform(get("/group/getByIdGroup/" + group.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void createGroup() {
        when(delegate.createGroup(anyString())).thenReturn(any(Group.class));
        String json = "{}";
        RequestBuilder builder = MockMvcRequestBuilders
                .post("/group/createGroup")
                .accept(MediaType.APPLICATION_JSON)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mvc.perform(builder).andReturn();
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }

    @Test
    @SneakyThrows
    public void removeGroup() {
        doNothing().when(delegate).removeGroup(group.getId());
        mvc.perform(get("/group/removeGroup/" + group.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void getStudentsByGroup() {
        Student student = Student.builder().build();
        given(delegate.getStudentsByGroup(any(Long.class))).willReturn(Arrays.asList(student));

        mvc.perform(get("/group/getStudentsByGroup/" + group.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void getAllGroup() {
        given(delegate.getAllGroup()).willReturn(Arrays.asList(group));
        mvc.perform(get("/group/getAllGroup")
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk());
    }
}