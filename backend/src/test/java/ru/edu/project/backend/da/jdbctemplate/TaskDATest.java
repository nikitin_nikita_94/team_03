package ru.edu.project.backend.da.jdbctemplate;

import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.model.TaskScore;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.MockitoAnnotations.openMocks;
import static ru.edu.project.backend.da.jdbctemplate.TaskDA.QUERY_FOR_ID;
import static ru.edu.project.backend.da.jdbctemplate.TaskDA.QUERY_FOR_STUDENT_ID;
import static ru.edu.project.backend.da.jdbctemplate.TaskDA.QUERY_FOR_UPDATE;

public class TaskDATest {

    public static final Long TASK_ID = 111L;
    public static final Long STUDENT_ID = 11L;
    public static final String TASK_CONTENT = "Some task content";
    public static final String COMMENT = "Some comment";
    public static final Timestamp CREATED_TIME = new Timestamp(1L);
    public static final Timestamp LAST_ACTION_TIME = new Timestamp(2L);
    public static final Timestamp CHECK_TIME = new Timestamp(3L);
    public static final Long SCORE = 2L;

    @Mock
    JdbcTemplate jdbcTemplate;

    @Mock
    NamedParameterJdbcTemplate jdbcNamed;

    @InjectMocks
    private TaskDA taskDA;

    SimpleJdbcInsert jdbcInsert;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
        jdbcInsert = Mockito.mock(SimpleJdbcInsert.class);
        Mockito.when(jdbcInsert.withTableName(any())).thenReturn(jdbcInsert);
        Mockito.when(jdbcInsert.usingGeneratedKeyColumns(any())).thenReturn(jdbcInsert);
        taskDA.setJdbcInsert(jdbcInsert);
    }

    @Test
    @SneakyThrows
    public void getStudentTasks() {
        ResultSet resultSetMock = Mockito.mock(ResultSet.class);
        Long studentId = 11L;
        List<TaskInfo> expectedResult = new ArrayList();

        Long taskId = 111L;
        Mockito.when(resultSetMock.getLong("id")).thenReturn(taskId);
        Mockito.when(resultSetMock.getLong("student_id")).thenReturn(studentId);
        Mockito.when(resultSetMock.getString("task_content")).thenReturn("contentMock");
        Mockito.when(resultSetMock.getString("comment")).thenReturn("commentMock");
        Timestamp created_time = new Timestamp(1L);
        Mockito.when(resultSetMock.getTimestamp("created_time")).thenReturn(created_time);
        Timestamp last_time = new Timestamp(2L);
        Mockito.when(resultSetMock.getTimestamp("last_action_time")).thenReturn(last_time);
        Timestamp check_time = new Timestamp(3L);
        Mockito.when(resultSetMock.getTimestamp("check_time")).thenReturn(check_time);
        Mockito.when(resultSetMock.getLong("score")).thenReturn(1L);
        Mockito.when(jdbcTemplate.query(eq(QUERY_FOR_STUDENT_ID), any(RowMapper.class), eq(studentId))).thenAnswer(invocation -> {
            RowMapper<TaskInfo> rowMapper = invocation.getArgument(1, RowMapper.class);
            expectedResult.add(rowMapper.mapRow(resultSetMock, 1));
            return expectedResult;
        });

        List<TaskInfo> list = taskDA.getStudentTasks(studentId);

        Mockito.verify(jdbcTemplate).query(eq(QUERY_FOR_STUDENT_ID), any(RowMapper.class), eq(studentId));

        assertEquals(1, list.size());

        TaskInfo info = list.get(0);
        assertEquals(taskId, info.getId());
        assertEquals(studentId, info.getStudentId());
        assertEquals("contentMock", info.getTaskContent());
        assertEquals("commentMock", info.getComment());
        assertEquals(created_time, info.getCreatedAt());
        assertEquals(last_time, info.getLastActionAt());
        assertEquals(check_time, info.getCheckAt());
        assertEquals(Optional.of(1L), Optional.ofNullable(info.getScore().getScoreCode()));
    }

    @Test
    @SneakyThrows
    public void getById() {
        Long taskId = 11L;

        ResultSet resultSetMock = configResultSetMock();
        Mockito.when(jdbcTemplate.query(eq(QUERY_FOR_ID), any(ResultSetExtractor.class), eq(taskId))).thenAnswer(invocation -> {
            ResultSetExtractor<TaskInfo> resultSetExtractor = invocation.getArgument(1, ResultSetExtractor.class);
            return resultSetExtractor.extractData(resultSetMock);
        });
        TaskInfo info = taskDA.getById(taskId);
        assertNotNull(info);
        Mockito.verify(jdbcTemplate).query(eq(QUERY_FOR_ID), any(ResultSetExtractor.class), eq(taskId));

        assertEqualsForTask(info);
    }

    @Test
    @SneakyThrows
    public void saveUpdate() {
        TaskInfo taskInfo = TaskInfo.builder()
                .id(TASK_ID)
                .studentId(STUDENT_ID)
                .taskContent(TASK_CONTENT)
                .comment(COMMENT)
                .createdAt(CREATED_TIME)
                .lastActionAt(LAST_ACTION_TIME)
                .checkAt(CHECK_TIME)
                .score(TaskScore.byScoreCode(SCORE))
                .build();

        TaskInfo result = taskDA.save(taskInfo);
        Mockito.verify(jdbcNamed).update(eq(QUERY_FOR_UPDATE), Mockito.anyMap());
        assertEquals(taskInfo.getId(), result.getId());
    }

    @Test
    @SneakyThrows
    public void saveInsert() {
        TaskInfo taskInfo = TaskInfo.builder()
                .studentId(STUDENT_ID)
                .taskContent(TASK_CONTENT)
                .comment(COMMENT)
                .createdAt(CREATED_TIME)
                .lastActionAt(LAST_ACTION_TIME)
                .checkAt(CHECK_TIME)
                .score(TaskScore.byScoreCode(SCORE))
                .build();

        Mockito.when(jdbcInsert.executeAndReturnKey(Mockito.anyMap())).thenReturn(TASK_ID);

        TaskInfo result = taskDA.save(taskInfo);
        assertEquals(TASK_ID, result.getId());
        Mockito.verify(jdbcInsert).executeAndReturnKey(Mockito.anyMap());

    }

    private ResultSet configResultSetMock() throws SQLException {
        ResultSet resultSetMock = Mockito.mock(ResultSet.class);

        Mockito.when(resultSetMock.getLong("id")).thenReturn(TASK_ID);
        Mockito.when(resultSetMock.getLong("student_id")).thenReturn(STUDENT_ID);
        Mockito.when(resultSetMock.getString("task_content")).thenReturn(TASK_CONTENT);
        Mockito.when(resultSetMock.getString("comment")).thenReturn(COMMENT);
        Mockito.when(resultSetMock.getTimestamp("created_time")).thenReturn(CREATED_TIME);
        Mockito.when(resultSetMock.getTimestamp("last_action_time")).thenReturn(LAST_ACTION_TIME);
        Mockito.when(resultSetMock.getTimestamp("check_time")).thenReturn(CHECK_TIME);
        Mockito.when(resultSetMock.getLong("score")).thenReturn(SCORE);
        return resultSetMock;
    }

    private void assertEqualsForTask(TaskInfo info) {
        assertEquals(TASK_ID, info.getId());
        assertEquals(STUDENT_ID, info.getStudentId());
        assertEquals(TASK_CONTENT, info.getTaskContent());
        assertEquals(COMMENT, info.getComment());
        assertEquals(CREATED_TIME, info.getCreatedAt());
        assertEquals(LAST_ACTION_TIME, info.getLastActionAt());
        assertEquals(CHECK_TIME, info.getCheckAt());
        assertEquals(SCORE, info.getScore().getScoreCode());
    }
}
