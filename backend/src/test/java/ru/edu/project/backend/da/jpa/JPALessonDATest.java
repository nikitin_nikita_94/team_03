package ru.edu.project.backend.da.jpa;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.da.jpa.converter.LessonMapper;
import ru.edu.project.backend.da.jpa.entity.LessonEntity;
import ru.edu.project.backend.da.jpa.repository.LessonEntityRepository;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;


public class JPALessonDATest {


    @Mock
    private LessonEntityRepository repo;

    @Mock
    private LessonMapper mapper;

    @InjectMocks
    private JPALessonDA lessonDA;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

        @Test
        public void getById () {
            Long lessonId = 11L;
            LessonEntity lessonEntity = new LessonEntity();
            Lesson lesson = Lesson
                    .builder()
                    .title("title")
                    .description("description")
                    .issue("issue")
                    .build();

            when(repo.findById(lessonId)).thenReturn(Optional.of(lessonEntity));
            when(mapper.map(lessonEntity)).thenReturn(lesson);

            assertEquals(lesson, lessonDA.getById(lessonId));
            verify(repo).findById(lessonId);
            verify(mapper).map(lessonEntity);

            when(repo.findById(lessonId)).thenReturn(Optional.empty());
            assertNotNull(lesson);
        }
    }
