package ru.edu.project.backend.controller;

import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.edu.project.backend.api.user.UserInfo;
import ru.edu.project.backend.da.UserDALayer;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.edu.project.backend.controller.UserController.EMPTY_USER;

@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private UserController controller;

    @Spy
    private UserDALayer service;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    @SneakyThrows
    public void register() {
        when(service.register(EMPTY_USER)).thenReturn(EMPTY_USER);
        controller.register(EMPTY_USER);
        mvc.perform(post("/user/register")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400));
    }

    @Test
    @SneakyThrows
    public void loadUserByUsername() {
        UserInfo userInfo = UserInfo.builder().id(1L).username("user").build();
        when(service.findByUsername(anyString())).thenReturn(userInfo);

        mvc.perform(get("/user/loadUserByUsername/" + userInfo.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}