package ru.edu.project.backend.da.jpa;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import ru.edu.project.backend.api.user.UserInfo;
import ru.edu.project.backend.app.DemoBackendApplication;
import ru.edu.project.backend.converter.UserInfoMapper;
import ru.edu.project.backend.da.jpa.entity.UserEntity;
import ru.edu.project.backend.da.jpa.repository.UserEntityRepository;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

@DataJpaTest
@ContextConfiguration(classes = DemoBackendApplication.class)
public class JPAUserDATest {

    @Mock
    private JPAUserDA jpaUserDA;

    @Mock
    private UserEntityRepository repo;

    @Mock
    private UserInfoMapper mapper;

    @Mock
    private UserEntity userEntity;

    @Mock
    private UserInfo userInfo;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void register() {
        when(mapper.map(userInfo)).thenReturn(userEntity);
        userEntity.setEnabled(true);

        when(repo.save(userEntity)).thenReturn(userEntity);
        when(mapper.map(userEntity)).thenReturn(userInfo);
        when(jpaUserDA.register(userInfo)).thenReturn(userInfo);

        UserInfo register = jpaUserDA.register(userInfo);
        assertEquals(userInfo,register);
        verify(jpaUserDA,timeout(1)).register(userInfo);
    }

    @Test
    public void findByUsername() {
        String userName = "userName";
        when(repo.findByUsername(userName)).thenReturn(userEntity);
        when(mapper.map(userEntity)).thenReturn(userInfo);
        when(jpaUserDA.findByUsername(userName)).thenReturn(userInfo);

        UserInfo byUsername = jpaUserDA.findByUsername(userName);
        assertEquals(userInfo,byUsername);
        verify(jpaUserDA,timeout(1)).findByUsername(userName);
    }
}