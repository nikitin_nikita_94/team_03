package ru.edu.project.backend.da.jpa;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.da.jpa.converter.TaskInfoMapper;
import ru.edu.project.backend.da.jpa.entity.TaskEntity;
import ru.edu.project.backend.da.jpa.repository.TaskEntityRepository;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class JPATaskDATest {

    @Mock
    private TaskEntityRepository repo;

    @Mock
    private TaskInfoMapper mapper;

    @InjectMocks
    private JPATaskDA taskDA;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void save() {
        TaskInfo draft = TaskInfo.builder().build();
        TaskEntity entity = new TaskEntity();

        when(mapper.map(draft)).thenReturn(entity);
        when(repo.save(entity)).thenReturn(entity);
        when(mapper.map(entity)).thenReturn(draft);
        assertEquals(draft, taskDA.save(draft));
    }
}
